# Removes all files older then 24 hours
webcamdir=/path/to/webcamfolder
find $webcamdir/images/ -type f -mmin +720 -exec rm {} \;
