# Builds webm of history images
ffmpeg -y -framerate 24 -pattern_type glob -i $webcamdir'/images/CAM1/*.jpg' -c:v libvpx -pix_fmt yuv420p $webcamdir/images/CAM1-BUILD.webm
mv $webcamdir/images/CAM1-BUILD.webm $webcamdir/images/CAM1.webm
ffmpeg -y -framerate 24 -pattern_type glob -i $webcamdir'/images/CAM2/*.jpg' -c:v libvpx -pix_fmt yuv420p $webcamdir/images/CAM2-BUILD.webm
mv $webcamdir/images/CAM2-BUILD.webm $webcamdir/images/CAM2.webm
ffmpeg -y -framerate 24 -pattern_type glob -i $webcamdir'/images/CAM3/*.jpg' -c:v libvpx -pix_fmt yuv420p $webcamdir/images/CAM3-BUILD.webm
mv $webcamdir/images/CAM3-BUILD.webm $webcamdir/images/CAM3.webm
