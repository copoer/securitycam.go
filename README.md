# ESP32 Webcam Server

I got tired of trying to setup other NVR systems so I said "frigate" and I built this with the goal of having a very easy and low resource intensive web cam hub for all of my esp32 cameras.
Using a simple go server to handle the incoming images along with ffmpeg to create videos to view camera history. For the AI recognition it simply uses tfjs in the web browser client.

## Features

- Simple basic authentication
- AI image recognition
- Browser notification when object detected
- View camera record history
- View latest images

![Demo](assets/demo.png)

## Setup

1. Copy config.go.example to config.go and fill in config
2. Build and run server
3. Copy camera script to esp32 camera and fill out config variables
4. (Optional) Install ffmpeg
3. Run background scripts on cron job, one deletes old image files the other builds webm

## TODO

- Add some styles
- Make building videos faster
- Improve detection
