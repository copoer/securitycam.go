let lastNotificationTime = [];
let cocoSsdModel = false;
let detectionEnabled = {};

const t = (path, sep) => path.split(sep)[0] + '?' + Math.random();
const detect = async img => {
	const objects = await cocoSsdModel.detect(img);
	for (let object of objects) {
		const detectedLabel = object.class;
		const filterOut = document.querySelector("#objFilter").value;
		if (!filterOut.includes(detectedLabel) && object.score > 0.65)  {
			const imageName = getImageName(img.src).split(".")[0];
			if (!lastNotificationTime[imageName+detectedLabel] || Date.now() - lastNotificationTime[imageName+detectedLabel] > 2 * 60 * 1000) {
				sendNotification(imageName, detectedLabel, img);
				lastNotificationTime[imageName+detectedLabel] = Date.now();
			}
		}
	}
};
const sendNotification = (imageName, detectedLabel, img) => {
	if (Notification.permission === "granted") {
		const notification = new Notification("Camera Alert!", { body: `${detectedLabel} Detected in ${imageName}`, icon: img.src});
		notification.addEventListener("show", function() {
			document.getElementById('notif_sound').play();
		}, false);
		notification.onclick = () => { enlargeImg(img.id) };
	}
};
const getImageName = imgSrc => imgSrc.split("/").pop();

const toggleDetection = id => {
	const button = document.getElementById(id+"Button");
	const isEnabled = !detectionEnabled[id];

	if (isEnabled) {
		button.innerText = "Disable Detection " + id;
		if (Notification.permission !== "granted") {
			Notification.requestPermission().then(permission => {
				if (permission === "granted") {
					enableDetectionForImage(id);
				}
			});
		} else {
			enableDetectionForImage(id);
		}
	} else {
		button.innerText = "Enable Detection " + id;
		detectionEnabled[id] = false;
	}
};

const enableDetectionForImage = id => {
	detectionEnabled[id] = true;
};

const reloadImage = async id => {
	const img = document.getElementById(id);
	img.src = t(img.src, '?');
	if (detectionEnabled[id]) {
		await detect(img);
	}
};

document.addEventListener('DOMContentLoaded', async () => {
	cocoSsdModel = await cocoSsd.load();
	const images = document.querySelectorAll('img');
	images.forEach((image) => {
		const id = image.id;
		setInterval(() => reloadImage(id), 500);
	});
});

async function enableDetection() {
	Object.keys(detectionEnabled).forEach(id => {
		if (!detectionEnabled[id]) {
			toggleDetection(id);
		}
	});
}

function enlargeImg(id) {
	const images = document.querySelectorAll('img');
	images.forEach((image) => {
		if (image.id != id) {
			image.classList.remove("active");
		}
	});

	let img = document.getElementById(id);
	if (img.className.match(/\bactive\b/)) {
		img.classList.remove("active");
	} else {
		img.classList.add("active");
	}
}

