/*
 * https://github.com/espressif/esp32-camera
 * Source documentation
 */
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include "soc/soc.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_camera.h"
#include "camera.h"

static camera_config_t camera_config = {
  .ledc_channel = LEDC_CHANNEL_0;
  .ledc_timer = LEDC_TIMER_0;
  .pin_d0 = Y2_GPIO_NUM;
  .pin_d1 = Y3_GPIO_NUM;
  .pin_d2 = Y4_GPIO_NUM;
  .pin_d3 = Y5_GPIO_NUM;
  .pin_d4 = Y6_GPIO_NUM;
  .pin_d5 = Y7_GPIO_NUM;
  .pin_d6 = Y8_GPIO_NUM;
  .pin_d7 = Y9_GPIO_NUM;
  .pin_xclk = XCLK_GPIO_NUM;
  .pin_pclk = PCLK_GPIO_NUM;
  .pin_vsync = VSYNC_GPIO_NUM;
  .pin_href = HREF_GPIO_NUM;
  .pin_sccb_sda = SIOD_GPIO_NUM;
  .pin_sccb_scl = SIOC_GPIO_NUM;
  .pin_pwdn = PWDN_GPIO_NUM;
  .pin_reset = RESET_GPIO_NUM;
  .xclk_freq_hz = 20000000;
  .frame_size = FRAMESIZE_UXGA;
  .pixel_format = PIXFORMAT_JPEG;  // for streaming
  .grab_mode = CAMERA_GRAB_LATEST;
  .fb_location = CAMERA_FB_IN_PSRAM;
  .jpeg_quality = JPEG_QUALITY;
  .fb_count = FB_COUNT;
};

esp_err_t camera_init(){
  //power up the camera if PWDN pin is defined
  if(CAM_PIN_PWDN != -1){
    pinMode(CAM_PIN_PWDN, OUTPUT);
    digitalWrite(CAM_PIN_PWDN, LOW);
  }

  //initialize the camera
  esp_err_t err = esp_camera_init(&camera_config);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "Camera Init Failed");
    return err;
  }

  return ESP_OK;
}

esp_err_t camera_capture(){
  //acquire a frame
  camera_fb_t * fb = esp_camera_fb_get();
  if (!fb) {
    ESP_LOGE(TAG, "Camera Capture Failed");
    return ESP_FAIL;
  }
  //replace this with your own function
  process_image(fb->width, fb->height, fb->format, fb->buf, fb->len);

  //return the frame buffer back to the driver for reuse
  esp_camera_fb_return(fb);
  return ESP_OK;
}
