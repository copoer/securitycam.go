package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/gorilla/mux"
)

type PageVariables struct {
	Photos []string
	Videos []string
}

func renderTemplate(w http.ResponseWriter, tmpl string, data PageVariables) {
	if t, err := template.ParseFiles(tmpl); err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	} else if err = t.Execute(w, data); err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
	}
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "tmpl/login.html", PageVariables{})
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	username, password := r.FormValue("username"), r.FormValue("password")

	if storedPassword, ok := users[username]; ok && storedPassword == password {
		token := jwt.New(jwt.SigningMethodHS256)
		if tokenString, err := token.SignedString(secretKey); err != nil {
			log.Println("ERROR:", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		} else {
			cookie := http.Cookie{Name: "jwt", Value: tokenString}
			http.SetCookie(w, &cookie)
			http.Redirect(w, r, "/secure", http.StatusFound)
		}
		return
	}

	http.Error(w, "Unauthorized", http.StatusUnauthorized)
}

func securePage(w http.ResponseWriter, r *http.Request) {
	files, _ := filepath.Glob("images/*.jpg")
	videoFiles, _ := filepath.Glob("images/*.webm")
	renderTemplate(w, "tmpl/cameras.html", PageVariables{Videos: videoFiles, Photos: files})
}

func requireTokenMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if cookie, err := r.Cookie("jwt"); err != nil || cookie.Value == "" {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		} else if token, err := validateToken(cookie.Value); err != nil || !token.Valid {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
		} else {
			next.ServeHTTP(w, r)
		}
	}
}

func validateToken(tokenString string) (*jwt.Token, error) {
	if token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	}); err != nil {
		return nil, err
	} else {
		return token, nil
	}
}

func createFolder(folderPath string) error {
	if _, err := os.Stat(folderPath); os.IsNotExist(err) && os.MkdirAll(folderPath, 0755) != nil {
		return err
	}
	return nil
}

func saveImage(latest, timestamped string, image io.Reader) error {
	if newFile1, err := os.Create(latest); err != nil {
		return err
	} else if newFile2, err := os.Create(timestamped); err != nil {
		return err
	} else if _, err := io.Copy(io.MultiWriter(newFile1, newFile2), image); err != nil {
		return err
	}
	return nil
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	if authHeader := r.Header.Get("Authorization"); authHeader != uploadAuthKey {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	r.ParseMultipartForm(10 << 20)
	if image, _, err := r.FormFile("image"); err != nil {
		http.Error(w, "Unable to get the image from the form", http.StatusBadRequest)
	} else {
		defer image.Close()
		device, name := r.Header.Get("Device"), r.Header.Get("Device")+".jpg"
		err := createFolder(imageDir)
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}

		filePath := filepath.Join(imageDir, name)
		historyPath := filepath.Join(imageDir, device)
		err = createFolder(historyPath)

		currentTime := time.Now()
		unixTimestamp := strconv.FormatInt(currentTime.Unix(), 10)
		historyFilePath := filepath.Join(historyPath, unixTimestamp+".jpg")

		err = saveImage(filePath, historyFilePath, image)
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		fmt.Fprintf(w, "Image uploaded successfully as: %s\n", filePath)
	}
}

func fileHandler(w http.ResponseWriter, r *http.Request, prefix, dir string) {
	http.StripPrefix(prefix, http.FileServer(http.Dir(dir))).ServeHTTP(w, r)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/login", loginPage).Methods("GET")
	r.HandleFunc("/login", loginHandler).Methods("POST")
	r.HandleFunc("/secure", requireTokenMiddleware(securePage)).Methods("GET")
	r.HandleFunc("/upload", uploadHandler).Methods("POST")

	r.Handle("/assets/{file}", requireTokenMiddleware(func(w http.ResponseWriter, r *http.Request) {
		fileHandler(w, r, "/assets/", "assets")
	})).Methods("GET")

	r.Handle("/images/{file}", requireTokenMiddleware(func(w http.ResponseWriter, r *http.Request) {
		fileHandler(w, r, "/images/", "images")
	})).Methods("GET")

	srv := &http.Server{
		Addr:    ":7000",
		Handler: r,
	}

	err := srv.ListenAndServe()
	if err != nil {
		log.Fatal("Error starting server: ", err)
	}
}
