module securitycamhub

go 1.21.1

require (
	github.com/golang-jwt/jwt/v5 v5.0.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
)
